<?php

namespace App\Http\Controllers;

use App\Models\jurusan;
use App\Models\perusahaan;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\pembimbing;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (Auth()->user()->level == 'siswa') {
            # code...
            return redirect('/dashboard/siswa');
        } elseif (Auth()->user()->level == 'pembimbing') {
            # code...
            // $data = ['data' => user::all()->sortBy('name')->where('level', 'siswa')];
            // $pembimbing = ['pembimbing' => pembimbing::all()];
            return redirect('/dashboard/pembimbing');
        }
        return redirect('/adminHome');
    }
    public function ShowTables()
    {
        $siswa = User::all()->where('level', "siswa")->sortBy('name');
        $pembimbing = Pembimbing::all()->sortBy('name');
        $perusahaan = perusahaan::all()->sortBy('name');
        $jurusan = jurusan::all()->sortBy('name');
        return view(
            'ShowAllTables',
            [
                'siswa' => $siswa,
                'pembimbing' => $pembimbing,
                'perusahaan' => $perusahaan,
                'jurusan' => $jurusan
            ]
        );
    }
}
