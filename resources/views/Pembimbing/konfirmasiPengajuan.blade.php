@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card" >
             <h5 class="card-header">Konfirmasi Pengajuan PRAKERIN</h5>
            <div class="card-body">
                <form action="/konfirmasiPengajuan" method="post">
                    @csrf
                @foreach ($data as $item)
                    <input type="hidden" name="id" value="{{ $item->id }}">
                    <div class="mb-3">
                      <label for="" class="form-label">NIS</label>
                      <input type="text"
                        class="form-control" name="" id="" aria-describedby="helpId" value="{{ $item->nis }}" readonly>
                    </div>
                    <div class="mb-3">
                      <label for="" class="form-label">Nama Lengkap</label>
                      <input type="text"
                        class="form-control" name="" id="" aria-describedby="helpId" value="{{ $item->name }}" readonly>
                    </div>
                    <div class="mb-3">
                      <label for="" class="form-label">Perusahaan yang di ajukan</label>
                      <input type="text"
                        class="form-control" name="" id="" aria-describedby="helpId" value="{{ $item->perusahaan->nama_perusahaan }}" readonly>
                    </div>
                    <div class="mb-3">
                      <label for="" class="form-label">Konfirmasi Balasan Dari perusahaan yang diajukan </label>
                      <select class="form-control" name="status" id="" style="height: 30px" required>
                        <option value=""></option>
                        <option value="Pengajuan Prakerin Disetujui">Disetujui</option>
                        <option value="Pengajuan Prakerin Ditolak">Ditolak</option>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="" class="form-label">Pilih Pembimbing </label>
                    <select class="form-control" name="pembimbing_id" id="" style="height: 30px" required>
                        <option value=""></option>
                        @foreach ($pembimbing as $item)
                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                        @endforeach
                      </select>
                    </div>
                @endforeach
                <button type="submit" class="btn btn-primary">Submit</button>
                </form>
                </div>
            </div>        
        </div>
    </div>

@endsection