@extends('layouts.app')

@section('content')
<div class="container">
    <form action="/editProfile" method="post">
        @csrf
        <div class="card">
        <div class="card-body">
         <h3>Profile</h3>
            <input type="hidden" name="id" value="{{ Auth()->user()->id }}">
            <div class="mb-3">
            <label for="" class="form-label">Nama Lengkap</label>
            <input type="text" class="form-control" name="name" id="" value="{{ Auth()->user()->name }}">
            </div>
            <div class="mb-3">
            <label for="" class="form-label">NIS</label>
            <input type="text" class="form-control" name="nis" id="" value="{{ Auth()->user()->nis }}">
            </div>
            <div class="mb-3">
            <label for="" class="form-label">Kelas</label>
            <select class="form-control" name="kelas" id="" style="height: 40px" readonly>
              <option selected value="{{ Auth()->User()->kelas }}">{{ Auth()->User()->kelas }}</option>
              {{-- <option value="X">X</option>
              <option value="XI">XI</option>
              <option value="XII">XII</option> --}}
            </select>
            </div>
            <div class="mb-3">
            <label for="" class="form-label">Jurusan</label>
            <select class="form-control" name="jurusan" id="" style="height: 40px" readonly>
              <option selected value="{{ Auth()->User()->jurusan }}" >{{ Auth()->User()->jurusan->jurusan }}</option>
              {{-- <option value="Reakayasa Perangka Lunak">Reakayasa Perangka Lunak</option>
              <option value="Teknik Mesin">Teknik Mesin</option>
              <option value="Otomotif">Otomotif</option>
              <option value="Listrik">Listrik</option> --}}
            </select>
            </div>
            <div class="mb-3">
              <label for="" class="form-label">Pembimbing</label>
              <input type="text" class="form-control" name="pembimbing" id="" value="Belum Memiliki Pembimbing" readonly> 
              @if (Auth()->User()->pembimbing_id == null)
              @else
              <input type="text" class="form-control" name="pembimbing" id="" value="{{ Auth()->user()->pembimbing->name }}" readonly>
              @endif
            </div>
            <div class="mb-3">
            <label for="" class="form-label">Foto</label>
              <img width="150px" src="\foto_users\{{ Auth()->User()->foto_users }}">
            </div>
            
        </div>
        </div>
        <div class="card">
        <div class="card-body">
         <h3>User Information</h3>
            <div class="mb-3">
              <label for="" class="form-label">Username</label>
              <input type="text" class="form-control" name="username" value="{{ Auth()->User()->username }}">
            </div>
            <div class="mb-3">
              <label for="" class="form-label">email</label>
              <input type="email" class="form-control" name="email"  value="{{ Auth()->User()->email }}">
            </div>
            <div class="mb-3">
              <a href="/gantiPassword">Ganti Password</a>
            </div>
        </div>
        </div>
          <button type="submit" class="btn btn-primary">Submit</button>
 
    </form>
    <button onclick="kembali()" class="btn btn-danger">Kembali</button>
                <script>function kembali(){
                    window.history.back();
                }</script>
</div>

@endsection